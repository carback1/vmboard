import React, { Component } from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

// TODO: redux or some other state container paradigm
let recorder, chunks;

function uploadVideo() {
  console.log('# of Chunks: ' + chunks.length)
  var blob = new Blob(chunks, {type: 'video/webm' })
  var fd = new FormData();
  fd.append('fname', 'uploadvideo.mp4');
  fd.append('data', blob);
  axios.post('/upload', fd)
    .then(function (res) {
      console.log('Success!');
      console.log(res);
    })
    .catch(function (error) {
      console.log('Failure!');
      console.log(error);
    });
}

class VideoElement extends React.Component {
  render() {
    return (<video id="localStream" autoplay muted></video>);
  }
}

class VideoRecorder extends React.Component {
  // States are 'asking', 'waiting', 'started' (to record/recording)
  // Actions are showLocalVid (asking -> waiting),
  //             startRecording (waiting -> started),
  //             stopRecording (started -> waiting)
  constructor() {
    super();
    this.state = {
      value: 'asking'
    };
  }

  showLocalVid() {
    let localStream = document.getElementById('localStream');
    let mediaopts = {
      tag: 'video',
      type: 'video/webm',
      ext: '.mp4',
      streams: {video: true, audio: true}
    };
    navigator.mediaDevices.getUserMedia(mediaopts.streams)
      .then(stream => {
        recorder = new MediaRecorder(stream);
        recorder.ondataavailable = e => {
          chunks.push(e.data);
          if(recorder.state == 'inactive') uploadVideo();
        };
        localStream.srcObject = stream;
        localStream.autoplay = true;
        console.dir(localStream);
      })
  }

  startRecording() {
    chunks = [];
    recorder.start();
  }

  stopRecording() {
    if (recorder) {
      recorder.stop();
      let localStream = document.getElementById('localStream');
      localStream.srcObject = null;
    }
  }

  render() {
    let message, next_state;
    if (this.state.value === 'asking') {
      message = 'Start New Message';
      next_state = 'waiting';
      this.stopRecording();
    }
    if (this.state.value === 'waiting') {
      message = 'Start Recording';
      next_state = 'started';
      this.showLocalVid();
    }
    if (this.state.value === 'started') {
      message = 'Stop Recording';
      next_state = 'asking';
      this.startRecording();
    }

    return (
        <button className="btn btn-default"
                onClick={() => this.setState({value: next_state})}>
          {message}
        </button>
    );
  }
}

class VideoMessage extends React.Component {
  render() {
    return (
        <video controls loop src={this.props.value}></video>
    );
  }
}

class MessageList extends React.Component {
  constructor() {
    super();
    this.state = {
      messages: []
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.poll(),
      1000
    );
  }

  poll() {
    axios.get(`messages`)
      .then(res => {
        this.setState({ messages: res.data });
      });
  }

  render() {
    const vms = this.state.messages.map((message, i) => {
        return (
            <div className="videomsg"><VideoMessage value={message} /></div>
        );
    });

    return <div>{vms}</div>;
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Video Message Board</h2>
        </div>
        <p className="App-intro">
        <VideoRecorder />
        <br />
        <VideoElement />
        <br />
        <MessageList />
        <br />
        </p>
      </div>
    );
  }
}

export default App;
