'use strict';

var express = require('express')
var fs = require('fs')
var multer  = require('multer')

// Import and configure our file upload middleware to save to 'uploads' folder
var upload = multer({ dest: 'uploads/' })


var app = express()
// Serve files in the React 'build' folder by default
app.use(express.static('build'));
// Serve uploads directly from the uploads folder
app.use('/uploads', express.static('uploads'));

// The messages route returns the available video messages
// as a simple json array
// TODO: Order by date
app.get('/messages', function(req, res) {
  var message_files = []
  fs.readdirSync('uploads').forEach(function(file) {
    if (file.length != 32) {
      return
    }
    file = 'uploads/'+file;
    var stat = fs.statSync(file);
    if (stat && !stat.isDirectory()) {
      message_files.push(file);
    }
  });
  res.end(JSON.stringify(message_files))
});

// Upload route that responds to post calls
app.post("/upload", upload.any(), function(req, res) {
  console.log("Processing upload...");
  console.dir(req);
  res.end(JSON.stringify(req.files));
});

app.listen(3000, function () {
  console.log('Listening on port 3000')
})
